{{- define "project" -}}
{{- printf "%s-%s-%s" .Values.global.entity .Chart.Name .Values.global.env }}
{{- end }}

{{- define "projectId" -}}
{{- printf "%s-%s-%s%s" .Values.global.entity .Chart.Name .Values.global.env .Values.global.salt }}
{{- end }}

{{- define "fleet.project" -}}
{{- printf "%s-fleet-%s" .Values.global.entity .Values.global.env -}}
{{- end }}

{{- define "fleet.projectId" -}}
{{- printf "%s-fleet-%s%s" .Values.global.entity .Values.global.env .Values.global.salt -}}
{{- end }}

{{- define "infragitops.project" -}}
{{- printf "%s-infragitops-%s" .Values.global.entity .Values.global.env -}}
{{- end }}

{{/*
{{- define "infragitops.projectId" -}}
{{- printf "%s-infragitops-%s%s" .Values.global.entity .Values.global.env .Values.global.salt -}}
{{- end }}
*/}}

{{- define "infragitops.projectId" -}}
{{- printf "%s-infragitops-%s%s" .Values.global.entity .Values.global.env .Values.global.infragitops.salt -}}
{{- end }}

{{- define "svpc.project" -}}
{{- printf "%s-sharedvpc-%s" .Values.global.entity .Values.global.env -}}
{{- end }}

{{- define "svpc.projectId" -}}
{{- printf "%s-sharedvpc-%s%s" .Values.global.entity .Values.global.env .Values.global.salt -}}
{{- end }}
