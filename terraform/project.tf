module "project-infragitops" {

  source  = "terraform-google-modules/project-factory/google"
  version = "~> 14.2"

  name = "${var.entity}-infragitops-${var.env}"

  labels = {
    "entity"          = "pgm"
    "env"             = "dev"
    "managed-by-cnrm" = "true"
    "role"            = "infragitops"
  }

  random_project_id = true
  org_id            = var.org_id
  folder_id         = var.folder
  billing_account   = var.billing_account

  default_service_account = "keep"

  activate_apis = [
    "artifactregistry.googleapis.com",
    "cloudbilling.googleapis.com",
    "cloudkms.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "iam.googleapis.com",
    "krmapihosting.googleapis.com",
    "monitoring.googleapis.com",
    "secretmanager.googleapis.com",
    "serviceusage.googleapis.com",
  ]

}
