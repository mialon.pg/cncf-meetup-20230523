# Terraform Doc

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| kubectl | ~> 1.14.0 |

## Providers

| Name | Version |
|------|---------|
| google | 4.66.0 |
| google-beta | 4.66.0 |
| kubectl | 1.14.0 |
| random | 3.5.1 |
| terraform | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| cloud\_router | terraform-google-modules/cloud-router/google | ~> 4.0 |
| oci\_repo\_workload\_identity | terraform-google-modules/kubernetes-engine/google//modules/workload-identity | n/a |
| project-infragitops | terraform-google-modules/project-factory/google | ~> 14.2 |
| secret-manager | GoogleCloudPlatform/secret-manager/google | ~> 0.1 |
| vpc | terraform-google-modules/network/google | ~> 5.0 |

## Resources

| Name | Type |
|------|------|
| [google-beta_google_project_service_identity.secretmanager_identity](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_project_service_identity) | resource |
| [google_artifact_registry_repository.oci-repo](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/artifact_registry_repository) | resource |
| [google_billing_account_iam_member.user](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/billing_account_iam_member) | resource |
| [google_folder_iam_member.folder_admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/folder_iam_member) | resource |
| [google_folder_iam_member.folder_owner](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/folder_iam_member) | resource |
| [google_folder_iam_member.project_creator](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/folder_iam_member) | resource |
| [google_folder_iam_member.shared_vpc_admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/folder_iam_member) | resource |
| [google_kms_crypto_key.crypto_key](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_crypto_key) | resource |
| [google_kms_crypto_key_iam_member.sm_sa_encrypter_decrypter](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_crypto_key_iam_member) | resource |
| [google_kms_key_ring.key_ring](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_key_ring) | resource |
| [google_project_iam_member.config_controller_owner](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [kubectl_manifest.root_sync](https://registry.terraform.io/providers/gavinbunney/kubectl/latest/docs/resources/manifest) | resource |
| [random_id.random_kms_suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [terraform_data.config_controller](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/resources/data) | resource |
| [google_container_cluster.config_controller](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/container_cluster) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| billing\_account | GCP Billing Account | `string` | n/a | yes |
| entity | Entity prefix | `string` | `"pgm"` | no |
| env | Name of the environement | `string` | `"dev"` | no |
| folder | Folder number for Shared VPC creation permissions | `string` | n/a | yes |
| org\_id | GCP organization | `string` | n/a | yes |
| region | Region for the resources | `string` | `"europe-west1"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->
