variable "org_id" {
  type        = string
  description = "GCP organization"
}

variable "folder" {
  type        = string
  description = "Folder number for Shared VPC creation permissions"
}

variable "billing_account" {
  type        = string
  description = "GCP Billing Account"
}

variable "region" {
  type        = string
  description = "Region for the resources"
  default     = "europe-west1"
}

variable "entity" {
  type        = string
  description = "Entity prefix"
  default     = "pgm"
}

variable "env" {
  type        = string
  default     = "dev"
  description = "Name of the environement"
}
