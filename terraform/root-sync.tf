# Root-Sync
# This will instruct config-controller to apply the resources defined in an OCI image
# contains in an Artifact Registry.

terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.14.0"
    }
  }
}

# We Adopt the GKE Autopilot created by the local exec step,
# to be able to reuse it during root-sync creation.
# The name of config-controller GKE Autopilot is deterministic:
# krmapihost-${config-controller-name}
data "google_container_cluster" "config_controller" {
  name     = "krmapihost-config-controller"
  location = var.region
  project  = module.project-infragitops.project_id

  depends_on = [terraform_data.config_controller]
}

provider "kubectl" {
  host                   = "https://${data.google_container_cluster.config_controller.endpoint}"
  cluster_ca_certificate = base64decode(data.google_container_cluster.config_controller.master_auth[0].cluster_ca_certificate)
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args        = []
    command     = "gke-gcloud-auth-plugin"
  }
  load_config_file = false
}

# This the main root-sync that will bootstrap Config-Controller
# Notice the service account root-reconciler-infragitops
resource "kubectl_manifest" "root_sync" {
  yaml_body = <<EOF
---
apiVersion: configsync.gke.io/v1beta1
kind: RootSync
metadata:
  name: infragitops
  namespace: config-management-system
spec:
  sourceType: oci
  sourceFormat: unstructured
  oci:
    image: ${var.region}-docker.pkg.dev/${module.project-infragitops.project_id}/oci/infragitops
    auth: gcpserviceaccount
    gcpServiceAccountEmail: root-reconciler-infragitops@${module.project-infragitops.project_id}.iam.gserviceaccount.com
EOF

  depends_on = [terraform_data.config_controller]
}

# Artifact registry
# Create the oci repo for oci images
resource "google_artifact_registry_repository" "oci-repo" {
  labels = {
    "managed-by-cnrm" = "true"
  }
  location      = var.region
  project       = module.project-infragitops.project_id
  repository_id = "oci"
  description   = "OCI registry for infrastructure deployment"
  format        = "DOCKER"

  depends_on = [module.project-infragitops]
}

# Grant permission with Workload Identity to Google service account: root-reconciler-infragitops to
# read the oci contains in the artifact registry.
# Notice: The RootSync object create the appropriate SA in the Config-Controller GKE Autopilot
# cluster, for this reason we use an existing k8s SA and we don't want to add the already
# existing annotation.
module "oci_repo_workload_identity" {
  source              = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  name                = "root-reconciler-infragitops"
  namespace           = "config-management-system"
  cluster_name        = "krmapihost-config-controller"
  project_id          = module.project-infragitops.project_id
  location            = var.region
  roles               = ["roles/artifactregistry.reader"]
  annotate_k8s_sa     = false
  use_existing_k8s_sa = true

  depends_on = [terraform_data.config_controller]
}
