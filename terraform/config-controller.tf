# At this time, there is no existing terraform resources for config-controller instances
# so this is a dirty local-exec... Sorry for that.
resource "terraform_data" "config_controller" {
  provisioner "local-exec" {
    command = "gcloud anthos config controller create config-controller --location=${var.region} --full-management --project=${module.project-infragitops.project_id} --network=${module.vpc.network_name} --subnet=${var.env}-config-controller --services-named-range=${var.env}-gke-subnet-secondary-svc --cluster-named-range=${var.env}-gke-subnet-secondary-pod"
  }

  depends_on = [
    module.vpc.module,
    module.project-infragitops,
  ]
}
