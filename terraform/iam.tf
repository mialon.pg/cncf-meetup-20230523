# Grant Owner permission to the config-controller default service account
# on all the projects that it will manage.
resource "google_project_iam_member" "config_controller_owner" {
  project = module.project-infragitops.project_id
  role    = "roles/owner"
  member  = "serviceAccount:service-${module.project-infragitops.project_number}@gcp-sa-yakima.iam.gserviceaccount.com"

  depends_on = [terraform_data.config_controller]
}

# Grant permission to the config-controller default service account,
#   - shared VPC admin on the folder that contains the projects
#   - folder Viewer to list projects
resource "google_folder_iam_member" "shared_vpc_admin" {
  folder = var.folder
  role   = "roles/compute.xpnAdmin"
  member = "serviceAccount:service-${module.project-infragitops.project_number}@gcp-sa-yakima.iam.gserviceaccount.com"

  depends_on = [terraform_data.config_controller]
}

resource "google_folder_iam_member" "folder_owner" {
  folder = var.folder
  role   = "roles/owner"
  member = "serviceAccount:service-${module.project-infragitops.project_number}@gcp-sa-yakima.iam.gserviceaccount.com"

  depends_on = [terraform_data.config_controller]
}
resource "google_folder_iam_member" "folder_admin" {
  folder = var.folder
  role   = "roles/resourcemanager.folderAdmin"
  member = "serviceAccount:service-${module.project-infragitops.project_number}@gcp-sa-yakima.iam.gserviceaccount.com"

  depends_on = [terraform_data.config_controller]
}

resource "google_folder_iam_member" "project_creator" {
  folder = var.folder
  role   = "roles/resourcemanager.projectCreator"
  member = "serviceAccount:service-${module.project-infragitops.project_number}@gcp-sa-yakima.iam.gserviceaccount.com"

  depends_on = [terraform_data.config_controller]
}

resource "google_billing_account_iam_member" "user" {
  billing_account_id = var.billing_account
  role               = "roles/billing.user"
  member             = "serviceAccount:service-${module.project-infragitops.project_number}@gcp-sa-yakima.iam.gserviceaccount.com"
}
