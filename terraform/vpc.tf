# This create a VPC with cloudNat
# the network is reused during the config-controller creation
module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 5.0"

  project_id   = module.project-infragitops.project_id
  network_name = "config-controller"
  routing_mode = "REGIONAL"

  subnets = [
    {
      subnet_name           = "${var.env}-config-controller"
      subnet_ip             = "172.25.31.128/25"
      subnet_region         = var.region
      subnet_private_access = "true"
    }
  ]

  secondary_ranges = {
    "${var.env}-config-controller" = [
      {
        range_name    = "${var.env}-gke-subnet-secondary-pod"
        ip_cidr_range = "172.23.224.0/19"
      },
      {
        range_name    = "${var.env}-gke-subnet-secondary-svc"
        ip_cidr_range = "172.24.252.0/22"
      },
    ]
  }

}

module "cloud_router" {
  source  = "terraform-google-modules/cloud-router/google"
  version = "~> 4.0"

  name    = "${var.env}-router"
  project = module.project-infragitops.project_id
  region  = var.region
  network = module.vpc.network_name

  nats = [{
    name = "${var.env}-nat-gateway"
  }]
}

