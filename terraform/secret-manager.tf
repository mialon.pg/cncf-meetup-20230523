# Create a secret-manager with cmek encryption
# It will be used to store infrastructure wide secrets
# such as Argocd credentials to access GitLab or
# Dynatrace keys.
# Infragitops access should only be granted to
# few operators
resource "random_id" "random_kms_suffix" {
  byte_length = 2
}

resource "google_kms_key_ring" "key_ring" {
  name       = "key-ring-${random_id.random_kms_suffix.hex}"
  location   = var.region
  project    = module.project-infragitops.project_id
  depends_on = [module.project-infragitops]
}

resource "google_kms_crypto_key" "crypto_key" {
  name     = "crypto-key-${random_id.random_kms_suffix.hex}s"
  key_ring = google_kms_key_ring.key_ring.id
}

resource "google_project_service_identity" "secretmanager_identity" {
  provider = google-beta
  project  = module.project-infragitops.project_id
  service  = "secretmanager.googleapis.com"
}

resource "google_kms_crypto_key_iam_member" "sm_sa_encrypter_decrypter" {
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"
  member        = "serviceAccount:${google_project_service_identity.secretmanager_identity.email}"
  crypto_key_id = google_kms_crypto_key.crypto_key.id
}

module "secret-manager" {
  source     = "GoogleCloudPlatform/secret-manager/google"
  version    = "~> 0.1"
  project_id = module.project-infragitops.project_id
  secrets = [
    {
      name        = "secret-kms-1"
      secret_data = "secret information"
    },
  ]
  user_managed_replication = {
    secret-kms-1 = [
      {
        location     = var.region
        kms_key_name = google_kms_crypto_key.crypto_key.id
      },
    ]
  }

  depends_on = [
    google_kms_crypto_key_iam_member.sm_sa_encrypter_decrypter
  ]
}
