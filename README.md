# ASM clusters with Config Controller


## About this POC

This poc contains:
* An example to bootstrap Config Controller with Terraform
* An example of Helm Chart to deploy two (or more) GKE Autopilot in an ASM multi cluster mesh

## How to use it

1. Create a gcp folder
1. Fill *terraform.tfvars* with org_id, folder and billing_account (see *variables.tf*)
1. Launch in the terraform folder
   ```
   terraform init
   terraform plan
   terraform apply
   ```
1. Then setup the *kustomization.yaml*, you won't be able to replace the projectNumber until the project are created by the OCI... sorry for that
1. You can have a look at deploy a bash script that will push the OCI for you. I use [nomos](https://cloud.google.com/anthos-config-management/docs/downloads#nomos_command), [kubeconform](https://github.com/yannh/kubeconform), and [flux](https://github.com/fluxcd/flux2) (since it is able to generate and push an OCI image with a single command).

If you want to use the CI pipeline on gitlab, you should create a service account and service account key and grant `roles/artifactregistry.writer` . Add the service account key as Variables in settings->CI/CD->Variables, name it: GOOGLE_APPLICATION_CREDENTIALS.


## Known issue:
```
     config-control   containercluster.container.cnrm.cloud.google.com/pgm-testitnamelength-dev-a9a5   InProgress   80852cf
        Update call failed: error applying desired state: summary: googleapi: Error 403: Google Compute Engine: Required 'compute.subnetworks.get' permission for 'projects/pgm-sharedvpc-dev-a9a5/regions/europe-west1/subnetwork
s/pgm-testitnamelength-dev-a9a5'., forbidden
```

Replace the projectNumbers in kustomization.yaml

## Documentation

https://cloud.google.com/service-mesh/docs/managed/provision-managed-anthos-service-mesh

https://mathieu-benoit.github.io/acm-workshop/index.html

https://thenewstack.io/4-core-principles-of-gitops/
